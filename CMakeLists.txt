project(ToxerCore)
cmake_minimum_required(VERSION 3.5.1)

if (NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

# setup version string
find_package(Git)
if(GIT_FOUND)
  execute_process(COMMAND ${GIT_EXECUTABLE} describe --tags --long --always --dirty=-d
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    RESULT_VARIABLE git_result
    OUTPUT_VARIABLE PROJECT_REVISION
    OUTPUT_STRIP_TRAILING_WHITESPACE
    ERROR_VARIABLE git_err
    )
  if (git_result EQUAL 0)
    message(STATUS "Toxer Git-Hash: ${PROJECT_REVISION}")
  else()
    message(FATAL_ERROR "Git error: ${git_err}")
  endif()
endif()
if(NOT PROJECT_REVISION)
  set(PROJECT_REVISION "DEVBUILD")
endif()
add_definitions(-DTOXER_VERSION="${PROJECT_REVISION}")

set (CMAKE_CXX_STANDARD 14)
set (CMAKE_CXX_STANDARD_REQUIRED ON)

find_package (Qt5 COMPONENTS Core Network Qml Gui Quick REQUIRED)

file (GLOB_RECURSE SCRIPTS "scripts/*.*")
add_custom_target(scripts SOURCES ${SCRIPTS})

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_library(TOX_CORE toxcore REQUIRED)

add_library(toxercore
  src/IToxNotify.cpp
  src/Private/ToxBootstrap.cpp
  src/Private/ToxerPrivate.cpp
  src/Private/ToxerRandom.cpp
  src/Private/ToxProfile.cpp
  src/Settings.cpp
  src/Toxer.cpp
  src/ToxerQml.cpp
  src/ToxTypes.cpp
  )

# configure library target "toxercore"
set_target_properties(toxercore PROPERTIES COMPILE_FLAGS
  "-W -Wall -fno-exceptions -fno-rtti -fvisibility=hidden"
  )
if(CMAKE_BUILD_TYPE EQUAL "MinSizeRel")
  set_target_properties(toxercore PROPERTIES COMPILE_FLAGS "-O3")
endif()
#include(CheckIPOSupported)
#check_ipo_supported(RESULT result)
#if(result)
if(FALSE)
  set_target_properties(toxercore PROPERTIES COMPILE_FLAGS "-flto")
  if("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    set_target_properties(toxercore PROPERTIES COMPILE_FLAGS
      "-fsanitize=cfi -fsanitize-cfi-cross-dso"
      )
  endif()
  set_property(TARGET toxercore PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
endif()

set_property (TARGET toxercore APPEND PROPERTY COMPILE_DEFINITIONS
  QT_NO_CAST_FROM_ASCII
  QT_NO_CAST_TO_ASCII
  QT_NO_CAST_FROM_BYTEARRAY
  )

target_include_directories (toxercore PRIVATE
	${TOX_CORE_INCLUDE_DIR}
  $<BUILD_INTERFACE:
  ${CMAKE_CURRENT_SOURCE_DIR}/src
  >)

target_link_libraries (toxercore
  Qt5::Quick
  ${TOX_CORE}
  sodium
  )

file(GLOB TOXERCORE_PUBLIC_HEADER "src/*.h")
set_target_properties(toxercore PROPERTIES PUBLIC_HEADER "${TOXERCORE_PUBLIC_HEADER}")

install(TARGETS toxercore
  LIBRARY        DESTINATION lib
  ARCHIVE        DESTINATION lib
  PUBLIC_HEADER  DESTINATION include/toxercore
  )
