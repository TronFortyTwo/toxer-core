/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2020 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "Settings.h"
#include "Private/ToxProfile.h"
#include "Private/ToxerPrivate.h"

#include <QStandardPaths>

/** @namespace Toxer
@class Settings
@brief Base class representing a file based Toxer configuration store.


@class ToxSettings
@brief Persistent Tox options.
*/

using Toxer::Settings;
using Toxer::ToxSettings;

Settings::Settings(Scope scope, const QString& filename)
  : s_{QStringLiteral("/home/phablet/.config/toxza.emanuelesorce/toxza.config"), QSettings::NativeFormat }
{
  switch (scope) {
  case QSettings::SystemScope:
    s_.setObjectName(QStringLiteral("ToxerSystemSettings"));
    break;
  case QSettings::UserScope:
    s_.setObjectName(QStringLiteral("ToxerUserSettings"));
    break;
  }
}

ToxSettings::ToxSettings(Scope scope, QObject* parent)
  : QObject(parent)
  , Settings(scope)
{
}

bool ToxSettings::ip6() const {
  auto v = s_.value(QLatin1String("tox/ipv6_enabled"));
  if (v.isNull()) {
    Tox_Options o;
    tox_options_default(&o);
    v = o.ipv6_enabled;
  }
  return v.toBool();
}

void ToxSettings::setIp6(bool enabled) {
  if (set(QLatin1String("tox/ipv6_enabled"), enabled, ip6())) {
    emit changed();
  }
}

void ToxSettings::resetIp6() { reset(QLatin1String("tox/ipv6_enabled")); }

bool ToxSettings::udp() const {
  auto v = s_.value(QLatin1String("tox/udp_enabled"));
  if (v.isNull()) {
    Tox_Options o; tox_options_default(&o);
    v = o.udp_enabled;
  }
  return v.toBool();
}

void ToxSettings::setUdp(bool enabled) {
  if (set(QLatin1String("tox/udp_enabled"), enabled, udp())) {
    emit changed();
  }
}

void ToxSettings::resetUdp() { reset(QLatin1String("tox/udp_enabled")); }

ToxTypes::Proxy ToxSettings::proxyType() const {
  auto v = s_.value(QLatin1String("tox/proxy_type"));
  if (v.isNull()) {
    Tox_Options o; tox_options_default(&o);
    v = ToxTypes::toQVariant(Toxer::fromTox(o.proxy_type));
  }
  return v.value<ToxTypes::Proxy>();
}

void ToxSettings::setProxyType(ToxTypes::Proxy type) {
  if (set(QLatin1String("tox/proxy_type"), ToxTypes::enumKey(type), ToxTypes::enumKey(proxyType()))) {
    emit changed();
  }
}
void ToxSettings::resetProxyType() { reset(QLatin1String("tox/proxy_type")); }

quint16 ToxSettings::proxyPort() const {
  auto v = s_.value(QLatin1String("tox/proxy_port"));
  if (v.isNull()) {
    Tox_Options o; tox_options_default(&o);
    v = o.proxy_port;
  }
  return static_cast<quint16>(v.toUInt());
}

void ToxSettings::setProxyPort(quint16 port) {
  if (set(QLatin1String("tox/proxy_port"), port, proxyPort())) {
    emit changed();
  }
}

void ToxSettings::resetProxyPort() { reset(QLatin1String("tox/proxy_port")); }

QString ToxSettings::proxyAddress() const {
  auto v = s_.value(QLatin1String("tox/proxy_host"));
  if (v.isNull()) {
    Tox_Options o; tox_options_default(&o);
    v = QString::fromUtf8(o.proxy_host);
  }
  return v.toString();
}

void ToxSettings::setProxyAddress(const QString& ip) {
  if (set(QLatin1String("tox/proxy_host"), ip, proxyAddress())) {
    emit changed();
  }
}

void ToxSettings::resetProxyAddress() { reset(QLatin1String("tox/proxy_host")); }
