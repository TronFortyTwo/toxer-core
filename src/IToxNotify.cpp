/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "IToxNotify.h"

#include "Private/ToxerPrivate.h"
#include "Private/ToxProfile.h"

/**
@class IToxFileNotifier
@brief interface for Tox file notifications

@fn IToxFileNotifier::on_avatar_request
@brief request by Tox friend to send his/her avatar

@fn IToxFileNotifier::on_cancelled
@brief cancel file transfer

@fn IToxFileNotifier::on_chunk
@brief received a chunk of a file in transfer

@fn IToxFileNotifier::on_file_request
@brief request by Tox friend to send a file

@fn IToxFileNotifier::on_file_transfer
@brief request a Tox friend to accept file transfer

@fn IToxFileNotifier::on_finished
@brief file transfer finished

@fn IToxFileNotifier::next_chunk
@brief ready to send the next chunk of a file in transfer

@fn IToxFileNotifier::on_paused
@brief the file transfer shall be paused

@fn IToxFileNotifier::on_resumed
@brief the file transfer shall be resumed


@class IToxFriendNotifier
@brief interface for Tox friend notifications

@fn on_added
@brief A friend has been invited and was added to the friend list.

@fn on_deleted
@brief A friend has been deleted from the friend list.

@fn on_name_changed
@brief A friend's name has changed.

@fn on_status_message_changed
@brief A friend's status message has changed.

@fn on_status_changed
@brief a friend's available status has changed.

@fn on_is_online_changed
@brief A friend's online status has changed.

@fn on_is_typing_changed
@brief A friend's typing status has changed.

@fn on_message
@brief A message was received from a friend.


@class IToxProfileNotifier
@brief interface for Tox profile notifications

@fn on_user_name_changed
@brief The profile user's name changed.

@fn on_is_online_changed
@brief The profile user's online status changed.

@fn on_status_message_changed
@brief The profile user's status message changed.

@fn on_status_changed
@brief The profile user's available status changed.

@fn on_friend_request
@brief A friend request has been received from another Tox friend.
*/

/** @brief IToxFriendNotifier abstract constructor */
IToxFriendNotifier::IToxFriendNotifier() {
  auto p = Toxer::activeProfile(); Q_ASSERT(p);
  p->addNotificationObserver(this);
}

/** @brief IToxFriendNotifier destructor */
IToxFriendNotifier::~IToxFriendNotifier() {
  auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    p->removeNotificationObserver(this);
  }
}

/** @brief IToxFriendsNotifier constructor */
IToxFriendsNotifier::IToxFriendsNotifier() {
  auto p = Toxer::activeProfile(); Q_ASSERT(p);
  p->addNotificationObserver(this);
}

/** @brief IToxFriendsNotifier destructor */
IToxFriendsNotifier::~IToxFriendsNotifier() {
  auto p = Toxer::activeProfile();
  if(Q_LIKELY(p)) {
    p->removeNotificationObserver(this);
  }
}

/** @brief IToxProfileNotifier constructor */
IToxProfileNotifier::IToxProfileNotifier() {
  auto p = Toxer::activeProfile(); Q_ASSERT(p);
  p->addNotificationObserver(this);
}

/** @brief IToxProfileNotifier destructor */
IToxProfileNotifier::~IToxProfileNotifier() {
  auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    p->removeNotificationObserver(this);
  }
}
