/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#include "ToxTypes.h"

#include <QMutex>
#include <QThread>
#include <QVector>

#if (QT_VERSION <= QT_VERSION_CHECK(5,9,0))
#include <functional>
#include <memory>
#endif

class IToxFileNotifier;
class IToxFriendNotifier;
class IToxFriendsNotifier;
class IToxProfileNotifier;
class Tox;

class ToxProfile final {
  class Private;
  class ToxEventLoop final : public QThread
  {
    friend class ToxProfile;
    friend class ToxProfile::Private;

  private:
    ToxEventLoop(Tox* _tox);
#if 0
    inline ~ToxEventLoop() override { qInfo("TEL destroyed"); }
#endif

    void bootstrap(quint8 bs_connections);
    inline void stop() {
      Q_ASSERT(QThread::currentThread() != this);
      QMutexLocker lock(&mutex_);
      active_ = false;
      quit(); // we don't actually use an event loop!
    }

    void run() final;

  private:
    mutable QMutex mutex_;
    Tox* tox_;
    bool active_;
  };

public:
  using Ptr = std::unique_ptr<ToxProfile>;

public:
  static bool create(const QString& name,
                     const QString& password,
                     const QString& toxAlias);

private:
  static Tox* createTox(const QByteArray& profileData);

public:
  using GetFunc = std::function<void (const Tox*)>;
  using SetFunc = std::function<void (Tox*)>;

public:
  ToxProfile(const QString& name, const QByteArray& profileData);
  ~ToxProfile();

  const QString& name() const;

  void start();
  void toxQuery(GetFunc query_func) const;
  void toxSet(SetFunc set_func);

  void addNotificationObserver(IToxFriendNotifier* notify);
  void removeNotificationObserver(IToxFriendNotifier* notify);
  void addNotificationObserver(IToxFriendsNotifier* notify);
  void removeNotificationObserver(IToxFriendsNotifier* notify);
  void addNotificationObserver(IToxProfileNotifier* notify);
  void removeNotificationObserver(IToxProfileNotifier* notify);

  // tox friends
  bool inviteFriend(const QString& toxId, const QString& message);
  void deleteFriend(quint32 friend_no);
  QString pk_str(quint32 friend_no) const;
  QString name(quint32 friend_no) const;
  QString status_message(quint32 friend_no) const;
  bool is_online(quint32 friend_no) const;
  ToxTypes::UserStatus availability(quint32 friend_no) const;
  bool is_typing(quint32 friend_no) const;

  // friend communicaton
  bool send_message(quint32 friend_no, const QString& message);
  void setFileNotifier(IToxFileNotifier* notify);
  void cancelFileTransfer(quint32 friend_no, quint32 file_no);
  void pauseFileTransfer(quint32 friend_no, quint32 file_no);
  void resumeFileTransfer(quint32 friend_no, quint32 file_no);
  void sendFile(uint32_t friend_no, const QString& name);

  // tox profile
  void setUserName(const QString& userName);
  void setStatusMessage(const QString& message);
  void setUserStatus(ToxTypes::UserStatus status);

  // this was private. Why not public
  void persist() const;
private:

  // profile notifiers
  void on_status_changed(ToxTypes::UserStatus status);
  void on_status_message_changed(const QString& message);
  void on_user_name_changed(const QString& userName);

  // friend notifiers
  void on_friend_added(quint32 friend_no);
  void on_friend_deleted(quint32 friend_no);

private:
  Private* p_;
  QVector<IToxProfileNotifier*> profileNotifiers_;
  QVector<IToxFriendNotifier*> friendNotifiers_;
  QVector<IToxFriendsNotifier*> friendsNotifiers_;
  std::unique_ptr<IToxFileNotifier> fileTransfer_;
};
