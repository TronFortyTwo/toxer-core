/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#include "ToxTypes.h"

#include "../../../toxcore/toxcore/tox.h"

#include <QStandardPaths>
#include <QStringBuilder>

class ToxProfile;

namespace Toxer
{

  class Private;

  constexpr auto toxErrStrFormat = "format mismatch";
  constexpr auto toxErrStrFriend = "friend not found";
  constexpr auto toxErrStrMalloc = "out of memory";
  constexpr auto toxErrStrNoConn = "friend not connected";
  constexpr auto toxErrStrNull = "One of the arguments was NULL.";
  constexpr auto toxErrStrOk = "no error";

  enum class Context : quint8
  {
    New,
    ChunkSend,
    Decrypt,
    Encrypt,
    FileControl,
    FileSend,
    FileReceive,
    FriendAdd,
    FriendDelete,
    FriendPK,
    Message,
    Query,
    Salt,
    SetInfo,
  };

  const char *err_str(int err, Context ctx);

  constexpr qint8 public_key_size() { return TOX_PUBLIC_KEY_SIZE; }
  constexpr qint8 nospam_size() { return TOX_NOSPAM_SIZE; }
  constexpr qint8 checksum_size() { return sizeof(quint16); }
  constexpr qint8 address_size() { return TOX_ADDRESS_SIZE; }
  constexpr qint8 address_raw_size() { return public_key_size() + nospam_size(); }

  template <typename ARRAY>
  constexpr size_t count(const ARRAY &arr) { return sizeof(arr) / sizeof(*arr); }

  inline QString profilesDir()
  {
    return QStandardPaths::writableLocation(
               // QStandardPaths::GenericConfigLocation) % QStringLiteral("/tox");
               QStandardPaths::GenericConfigLocation) %
           QStringLiteral("/toxza.emanuelesorce"); // TODO: don't hardcode this
  }
  ToxProfile *activeProfile();
  bool activate(const QString &profileName, const QString &password);
  QByteArray load(const QString &profileName, const QString &password);

  inline quint16 address_crc(const QByteArray &tox_addr)
  {
    const int last = tox_addr.size() - 1;
    Q_ASSERT(last >= checksum_size());
    auto c_tox_addr = reinterpret_cast<const quint8 *>(tox_addr.constData());
    auto chk = (c_tox_addr[last - 1] << 8);
    chk |= c_tox_addr[last];
    return static_cast<quint16>(chk);
  }
  inline quint16 bcc16(const char *arr, qint64 len)
  {
    Q_ASSERT(len % 2 == 0);
    quint16 chk = 0;
    Q_STATIC_ASSERT(sizeof(chk) == checksum_size());
    auto c_arr = reinterpret_cast<const quint8 *>(arr);
    for (auto i = 0; i < len; i += 2)
    {
      auto v = (c_arr[i] << 8);
      v |= c_arr[i + 1];
      chk ^= v;
    }

    return chk;
  }
  inline quint16 check_address(const QByteArray &tox_addr)
  {
    Q_ASSERT(tox_addr.size() == address_size());
    auto chk = bcc16(tox_addr.constData(), address_raw_size());
#if 0
  qDebug("addr    : %s", tox_addr.toHex().toUpper().constData());
  qDebug("addr-crc: %d", address_crc(tox_addr));
  qDebug("check   : %d", chk);
#endif
    return chk == address_crc(tox_addr);
  }

  bool isEncrypted(const char *data, int len);
  QByteArray decrypt(const QString &tox_key_id,
                     const QByteArray &data,
                     const QString &pass = {});
  QByteArray encrypt(const QString &tox_key_id,
                     const char *data,
                     size_t len,
                     const QString &pass = {});
  void removeToxKey(const QString &tox_key_id);

  inline const quint8 *to_tox(const char *data)
  {
    return reinterpret_cast<const uint8_t *>(data);
  }
  inline quint8 *to_tox(char *data) { return reinterpret_cast<uint8_t *>(data); }

  ToxTypes::ConnectionType fromTox(TOX_CONNECTION enumeration);
  TOX_CONNECTION toTox(ToxTypes::ConnectionType enumeration);

  ToxTypes::Proxy fromTox(TOX_PROXY_TYPE enumeration);
  TOX_PROXY_TYPE toTox(ToxTypes::Proxy enumeration);

  ToxTypes::Transport fromTox(TOX_FILE_CONTROL enumeration);
  TOX_FILE_CONTROL toTox(ToxTypes::Transport enumeration);

  ToxTypes::UserStatus fromTox(TOX_USER_STATUS enumeration);
  TOX_USER_STATUS toTox(ToxTypes::UserStatus enumeration);

} // namespace Toxer
