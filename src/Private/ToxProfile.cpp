/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "ToxProfile.h"

#include "IToxNotify.h"
#include "Private/ToxerPrivate.h"
#include "Settings.h"
#include "ToxBootstrap.h"
#include "ToxerRandom.h"

#include <QDir>
#include <QFile>

using Toxer::ToxSettings;

/** @class ToxProfile
@brief Tox profile Qt implementation.
*/

/** @class ToxMessage
@brief Simple Tox message parser
*/
class ToxMessage final : public QByteArray {
public:
  ToxMessage(const QString& str)
    : QByteArray(str.toUtf8().trimmed())
  {
    mt_ = startsWith("!") ? TOX_MESSAGE_TYPE_ACTION : TOX_MESSAGE_TYPE_NORMAL;
  }

public:
  TOX_MESSAGE_TYPE mt_;
};

/** @class ToxProfile::Private
@brief ToxProfile private data.
*/
class ToxProfile::Private final {
public:
  Private(Tox* tox, const QString& name)
    : tel_(new ToxEventLoop(tox))
    , name_(name)
  {
  }
  ~Private() {
    tel_->stop();
#if 0
    qInfo("Waiting on TEL");
#endif
    tel_->wait();
    delete tel_;
    // remove profile key from purse
    Toxer::removeToxKey(name_);
  }

  void persist() const {
    auto size = tox_get_savedata_size(tel_->tox_);
    auto data = new char[size];
    tox_get_savedata(tel_->tox_, Toxer::to_tox(data));
    QDir().mkpath(Toxer::profilesDir());
    QFile f(Toxer::profilesDir() %
            QString::fromUtf8("/%1.tox").arg(name_));
    if (f.open(QFile::WriteOnly)) {
      auto encrypted = Toxer::encrypt(name_, data, size);
      auto written = f.write(encrypted);
      Q_ASSERT(written == encrypted.size());
    } else {
      qWarning("Unable to save Tox profile data!");
    }
    delete[] data;
  }

public:
  ToxEventLoop* tel_;
  QString name_;
};

/** @brief Creates a Tox profile.
@param name         the profile name
@param password     the profile password
@param toxAlias     the user alias visible to Tox friends
*/
bool ToxProfile::create(const QString& name,
                        const QString& password,
                        const QString& toxAlias)
{
  QDir().mkpath(Toxer::profilesDir());
  QFile f(Toxer::profilesDir() % QString::fromUtf8("/%1.tox").arg(name));

  if (name.length() == 0) {
    qWarning("Tox profile name is of length 0");
    return false;
  }
  
  if (f.exists()) {
    qWarning("Existing Tox profile found at \"%s\"",
             qUtf8Printable(f.fileName()));
    return false;
  }

  if (f.open(QFile::WriteOnly)) {
    auto t = createTox({});
    if (t) {
      // set tox user name
      auto c_alias = toxAlias.toUtf8();
      tox_self_set_name(t,
                        Toxer::to_tox(c_alias.constData()),
                        static_cast<size_t>(c_alias.size()),
                        nullptr);

      // write  tox file
      auto size = tox_get_savedata_size(t);
      auto data = new char[size];
      tox_get_savedata(t, Toxer::to_tox(data));
      auto encrypted = Toxer::encrypt(name, data, size, password);
      if (encrypted.isEmpty()) {
        qWarning("Failed to encrypt Tox Profile!");
      } else {
        f.write(encrypted);
      }
      delete[] data;
      tox_kill(t);
      return true;
    }
    return false;
  } else {
    qWarning("Failed to create Tox profile %s.\nError message: %s",
             qUtf8Printable(name),
             qUtf8Printable(f.errorString()));
    return false;
  }
}

/** @brief Creates a Tox instance for the profile.
@param profileData  the saved profile state
@return the created Tox instance
*/
Tox* ToxProfile::createTox(const QByteArray& profileData)
{
  ToxSettings toxSettings;
  Tox_Options toxOpts;
  tox_options_default(&toxOpts);
  toxOpts.ipv6_enabled = toxSettings.ip6();
  toxOpts.udp_enabled = toxSettings.udp();

  if (!profileData.isEmpty()) {
    toxOpts.savedata_type = TOX_SAVEDATA_TYPE_TOX_SAVE;
    toxOpts.savedata_data = Toxer::to_tox(profileData.constData());
    toxOpts.savedata_length = static_cast<size_t>(profileData.size());
  }

  auto proxyType = toxSettings.proxyType();
  if (proxyType != ToxTypes::Proxy::None) {
    auto proxyAddr = toxSettings.proxyAddress().toUtf8();
    auto proxyPort = toxSettings.proxyPort();
    if (proxyAddr.length() > 255) {
      qWarning("The proxy address %s is too long.",
               qUtf8Printable(toxSettings.proxyAddress()));
    } else if (!proxyAddr.isEmpty() && toxSettings.proxyPort() > 0) {
      toxOpts.proxy_type = Toxer::toTox(proxyType);
      toxOpts.proxy_host = proxyAddr.constData();
      toxOpts.proxy_port = proxyPort;
    }
  }

  TOX_ERR_NEW err;
  auto tox = tox_new(&toxOpts, &err);
  if (Q_UNLIKELY(err)) {
    qWarning("Creation of Tox instance failed: %s",
             Toxer::err_str(err, Toxer::Context::New));
  }

  return tox;
}

ToxProfile::ToxEventLoop::ToxEventLoop(Tox* _tox)
  : QThread()
  , tox_(_tox)
  , active_(false)
{
  Q_ASSERT(tox_);
  setObjectName(QStringLiteral("ToxEventLoop"));
}

/** @brief Bootstraps randomly registered Tox nodes.
@param bs_connections   number of relay nodes used for Tox bootstrapping
*/
void ToxProfile::ToxEventLoop::bootstrap(quint8 bs_connections)
{
  Q_ASSERT(tox_);
  Q_ASSERT(!isRunning());

  // random bootstrap node selection
  constexpr quint32 bs_node_count = Toxer::count(bootstrap_nodes);
  qWarning("Bootstrapping with %d/%u random Tox nodes.", (bs_connections > bs_node_count ? bs_node_count : bs_connections), bs_node_count);
  auto err = TOX_ERR_BOOTSTRAP_OK;
  ToxerRandomValues bs_indexes(bs_connections, bs_node_count);
  Q_ASSERT(bs_connections == bs_indexes.size());
  for (auto bs_idx : bs_indexes) {
    auto d = bootstrap_nodes[bs_idx];

    qInfo("Bootstrap Tox node #%u: %s", bs_idx, d.address);
    tox_bootstrap(tox_, d.address, d.port, d.key, &err);
    if (err) {
      qWarning("Failed to bootstrap address %s. Code: %d", d.address, err);
    }

    tox_add_tcp_relay(tox_, d.address, d.port, d.key, &err);
    if (err) {
      qWarning("Failed to add TCP relay for %s. Code: %d", d.address, err);
    }
  }
}

void ToxProfile::ToxEventLoop::run()
{
  QMutexLocker locker(&mutex_);
  auto interval = tox_iteration_interval(tox_);
  active_ = true;
  locker.unlock();
  while (active_) {
    tox_iterate(tox_, nullptr);
    msleep(interval);
  }
  locker.relock();
  tox_kill(tox_);
}

ToxProfile::ToxProfile(const QString& name, const QByteArray& profileData)
  : p_(new Private(createTox(profileData), name))
{
  tox_callback_self_connection_status(p_->tel_->tox_,
                                      [](Tox*, TOX_CONNECTION status, void*)
  {
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    for (auto n : activeProfile->profileNotifiers_) {
      n->on_is_online_changed(status != TOX_CONNECTION_NONE);
    }
  });

  tox_callback_friend_request(p_->tel_->tox_,
                              [](Tox*, const uint8_t* pk,
                              const uint8_t* msg, size_t msg_len, void*)
  {
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    auto c_pk = QByteArray::fromRawData(reinterpret_cast<const char*>(pk),
                                        Toxer::public_key_size());
    auto c_msg = QString::fromUtf8(reinterpret_cast<const char*>(msg),
                                   static_cast<int>(msg_len));
    for (auto n : activeProfile->profileNotifiers_) {
      n->on_friend_request(c_pk, c_msg);
    }
  });

  tox_callback_friend_connection_status(p_->tel_->tox_,
                                        [](Tox*, uint32_t friend_no,
                                        TOX_CONNECTION status, void*)
  {
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    for (auto n : activeProfile->friendNotifiers_) {
      n->on_is_online_changed(friend_no, status != TOX_CONNECTION_NONE);
    }
  });

  tox_callback_friend_name(p_->tel_->tox_,
                           [](Tox*, uint32_t friend_no,
                           const uint8_t* name, size_t len, void*)
  {
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    auto nameStr = QString::fromUtf8(reinterpret_cast<const char*>(name),
                                     static_cast<int>(len));
    for (auto n : activeProfile->friendNotifiers_) {
      n->on_name_changed(friend_no, nameStr);
    }
  });

  tox_callback_friend_status_message(p_->tel_->tox_,
                                     [](Tox*, uint32_t friend_no,
                                     const uint8_t* message, size_t len,
                                     void*)
  {
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    auto messageStr = QString::fromUtf8(reinterpret_cast<const char*>(message),
                                        static_cast<int>(len));
    for (auto n : activeProfile->friendNotifiers_) {
      n->on_status_message_changed(friend_no, messageStr);
    }
  });

  tox_callback_friend_status(p_->tel_->tox_,
                             [](Tox*, uint32_t friend_no,
                             TOX_USER_STATUS status, void*)
  {
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    auto c_status = Toxer::fromTox(status);
    for (auto n : activeProfile->friendNotifiers_) {
      n->on_status_changed(friend_no, c_status);
    }
  });

  tox_callback_friend_message(p_->tel_->tox_, [](Tox*, uint32_t friend_no,
                              TOX_MESSAGE_TYPE type, const uint8_t* message,
                              size_t len, void*)
  {
    Q_UNUSED(type) // TODO: handle message type
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    auto messageStr = QString::fromUtf8(reinterpret_cast<const char*>(message),
                                        static_cast<int>(len));
    for (auto n : activeProfile->friendNotifiers_) {
      n->on_message(friend_no, messageStr);
    }
  });

  tox_callback_file_chunk_request(p_->tel_->tox_,
                                  [](Tox*, uint32_t friend_no, uint32_t file_no,
                                  uint64_t pos, size_t len, void*)
  {
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    if (Q_LIKELY(activeProfile->fileTransfer_)) {
      auto ft = activeProfile->fileTransfer_.get();
      ft->on_next_chunk(friend_no, file_no, pos, len);
    } else {
      activeProfile->cancelFileTransfer(friend_no, file_no);
    }
  });
  tox_callback_file_recv(p_->tel_->tox_,
                         [](Tox*, uint32_t friend_no, uint32_t file_no,
                         uint32_t kind, uint64_t file_size,
                         const uint8_t* filename, size_t filename_len, void*)
  {
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    if (Q_LIKELY(activeProfile->fileTransfer_)) {
      auto ft = activeProfile->fileTransfer_.get();
      auto c_filename = QString::fromUtf8(reinterpret_cast<const char*>(filename),
                                          static_cast<int>(filename_len));
      switch (kind) {
      case TOX_FILE_KIND_DATA:
        ft->on_file_request(friend_no, file_no, c_filename, file_size);
        return;
      case TOX_FILE_KIND_AVATAR:
        ft->on_avatar_request(friend_no, file_no, file_size);
        return;
      }

      qWarning("unknown Tox file type for %s: %d", qUtf8Printable(c_filename), kind);
      // TODO: cancel file transfer
    } else {
      activeProfile->cancelFileTransfer(friend_no, file_no);
    }
  });
  tox_callback_file_recv_chunk(p_->tel_->tox_, [](Tox*, uint32_t friend_no,
                               uint32_t file_no, uint64_t pos,
                               const uint8_t* data, size_t len, void*)
  {
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    Q_ASSERT(activeProfile->fileTransfer_);
    auto ft = activeProfile->fileTransfer_.get();
    if (len == 0) {
      ft->on_finished(friend_no, file_no);
    } else {
      auto c_data = reinterpret_cast<const char*>(data);
      ft->on_chunk(friend_no, file_no, pos, c_data, len);
    }
  });
  tox_callback_file_recv_control(p_->tel_->tox_,
                                 [](Tox*, uint32_t friend_no, uint32_t file_no,
                                 TOX_FILE_CONTROL tfc, void*)
  {
    auto activeProfile = Toxer::activeProfile();
    Q_ASSERT(activeProfile);
    Q_ASSERT(activeProfile->fileTransfer_);
    auto ft = activeProfile->fileTransfer_.get();
    switch (tfc) {
    case TOX_FILE_CONTROL_CANCEL:
      ft->on_cancelled(friend_no, file_no);
      return;
    case TOX_FILE_CONTROL_PAUSE:
      ft->on_paused(friend_no, file_no);
      return;
    case TOX_FILE_CONTROL_RESUME:
      ft->on_resumed(friend_no, file_no);
      return;
    }
    Q_ASSERT(false); // fallthrough
  });
}

ToxProfile::~ToxProfile() {
  delete p_;
}

/** @brief Returns the Tox profile's name.
@return   the Tox profile's name

The Tox profile name matches the file name to persist Tox data.
*/
const QString& ToxProfile::name() const {
  return p_->name_;
}

/** @brief Persists the active Tox profile.

This has to be called when Tox profile data changes. E.g. when a friend is added
or removed. The profile data is encrypted and persisted.
*/
void ToxProfile::persist() const
{
  if (QThread::currentThread() == p_->tel_) {
    p_->persist();
  } else {
    QMutexLocker lock(&p_->tel_->mutex_);
    p_->persist();
  }
}

/** @brief A friend was added to the Tox profile.
@param friend_no  the friend number
*/
void ToxProfile::on_friend_added(quint32 friend_no) {
  for (auto n : friendsNotifiers_) {
    n->on_added(friend_no);
  }
}

/** @brief A friend was removed from the Tox profile.
@param friend_no  the friend number
*/
void ToxProfile::on_friend_deleted(quint32 friend_no) {
  for (auto n : friendsNotifiers_) {
    n->on_deleted(friend_no);
  }
}

/** @brief Starts the profile's Tox loop in its own thread context. */
void ToxProfile::start() {
  if (!p_->tel_->active_) {
    p_->tel_->bootstrap(4);
    p_->tel_->start();
  }
}

/** @brief thread-safe Tox query
@param query_func   the query callback
@note The query callback does not change the current Tox state.
*/
void ToxProfile::toxQuery(ToxProfile::GetFunc query_func) const {
  QMutexLocker(&p_->tel_->mutex_);
  query_func(p_->tel_->tox_);
}

/** @brief thread-safe Tox set function
@param set_func   the query callback
*/
void ToxProfile::toxSet(ToxProfile::SetFunc set_func) {
  QMutexLocker locker(&p_->tel_->mutex_);
  set_func(p_->tel_->tox_);
}

/** @brief registers a Tox friend event notification observer
@param notify   the event notifier
*/
void ToxProfile::addNotificationObserver(IToxFriendNotifier* notify) {
  friendNotifiers_ << notify;
}

/** @brief removes a Tox friend event notification observer
@param notify   the event notifier
*/
void ToxProfile::removeNotificationObserver(IToxFriendNotifier* notify) {
  friendNotifiers_.removeAll(notify);
}

/** @brief registers a Tox friend event notification observer
@param notify   the event notifier
*/
void ToxProfile::addNotificationObserver(IToxFriendsNotifier* notify) {
  friendsNotifiers_ << notify;
}

/** @brief removes a Tox friend event notification observer
@param notify   the event notifier
*/
void ToxProfile::removeNotificationObserver(IToxFriendsNotifier* notify) {
  friendsNotifiers_.removeAll(notify);
}

/** @brief registers a Tox profile event notification observer
@param notify   the event notifier
*/
void ToxProfile::addNotificationObserver(IToxProfileNotifier* notify) {
  profileNotifiers_ << notify;
}

/** @brief removes a Tox profile event notification observer
@param notify   the event notifiier
*/
void ToxProfile::removeNotificationObserver(IToxProfileNotifier* notify) {
  profileNotifiers_.removeAll(notify);
}

/** @brief Invite a Tox friend to join your Tox network. */
bool ToxProfile::inviteFriend(const QString& toxId, const QString& message) {
  TOX_ERR_FRIEND_ADD err;
  quint32 out_fno;
  toxSet([&](Tox* tox) {
    auto addr = QByteArray::fromHex(toxId.toLower().toLatin1());
    auto msg = message.toUtf8();
    out_fno = tox_friend_add(tox, Toxer::to_tox(addr.constData()),
                             Toxer::to_tox(msg.constData()),
                             static_cast<size_t>(msg.length()), &err);
  });
  if (Q_UNLIKELY(err)) {
    qWarning("Could not invite Tox friend %s: %s",
             qPrintable(toxId),
             Toxer::err_str(err, Toxer::Context::FriendAdd));
    return false;
  } else {
    persist();
    on_friend_added(out_fno);
    return true;
  }
}

/** @brief Deletes a friend from your Tox friend list. */
void ToxProfile::deleteFriend(quint32 friend_no) {
  TOX_ERR_FRIEND_DELETE err;
  toxSet([&](Tox* tox) {
    tox_friend_delete(tox, friend_no, &err);
  });
  if (err) {
    qWarning("Could not remove Tox friend %i: %s", friend_no,
             Toxer::err_str(err, Toxer::Context::FriendDelete));
  } else {
    persist();
    on_friend_deleted(friend_no);
  }
}

/** @brief sends a message to a Tox friend */
bool ToxProfile::send_message(quint32 friend_no, const QString& message) {
  QMutexLocker locker(&p_->tel_->mutex_);
  ToxMessage m(message);
  TOX_ERR_FRIEND_SEND_MESSAGE err;
  tox_friend_send_message(p_->tel_->tox_, friend_no, m.mt_,
                          Toxer::to_tox(m.constData()),
                          static_cast<size_t>(message.length()), &err);
  if (Q_UNLIKELY(err)) {
    qWarning("Sending message to friend %d failed: %s",
             friend_no, Toxer::err_str(err, Toxer::Context::Message));
    return false;
  }
  return true;
}

/** @brief Returns the PK of a friend.
@param friend_no  the friend number
@returns the friend's PK; empty if friend_no is invalid
*/
QString ToxProfile::pk_str(quint32 friend_no) const
{
  QMutexLocker locker(&p_->tel_->mutex_);
  char pk[Toxer::public_key_size()];
  TOX_ERR_FRIEND_GET_PUBLIC_KEY err;
  tox_friend_get_public_key(p_->tel_->tox_, friend_no, Toxer::to_tox(pk), &err);
  if (Q_UNLIKELY(err)) {
    qWarning("Failed to get PK from friend %d: %s", friend_no,
             Toxer::err_str(err, Toxer::Context::FriendPK));
    return QString();
  }

  // convert to hex-string
  auto pk_raw = QByteArray::fromRawData(pk, Toxer::public_key_size());
  return QString::fromLatin1(pk_raw.toHex());
}

/** @brief Returns the name of a friend.
@param friend_no  the friend number
@returns the friend's name; empty if friend_no is invalid
*/
QString ToxProfile::name(quint32 friend_no) const
{
  QMutexLocker locker(&p_->tel_->mutex_);
  TOX_ERR_FRIEND_QUERY err;
  auto len = tox_friend_get_name_size(p_->tel_->tox_, friend_no, &err);
  if (Q_UNLIKELY(err)) {
    qWarning("Failed to get name length of friend %d: %s", friend_no,
             Toxer::err_str(err, Toxer::Context::Query));
    return QString();
  }

  auto out = new char[len];
  tox_friend_get_name(p_->tel_->tox_, friend_no, Toxer::to_tox(out), &err);
  if (Q_UNLIKELY(err)) {
    qWarning("Failed to get name of friend %d: %s", friend_no,
             Toxer::err_str(err, Toxer::Context::Query));
    // noreturn
  }
  QString outStr = QString::fromUtf8(out, static_cast<int>(len));
  delete[] out;
  return outStr;
}

/** @brief Returns the status message of a friend.
@param friend_no  the friend number
@returns the friend's status message; empty if friend_no is invalid
*/
QString ToxProfile::status_message(quint32 friend_no) const
{
  QMutexLocker locker(&p_->tel_->mutex_);
  TOX_ERR_FRIEND_QUERY err;
  auto len = tox_friend_get_status_message_size(p_->tel_->tox_, friend_no, &err);
  if (Q_UNLIKELY(err)) {
    qWarning("Failed to get status message length of friend %d: %s", friend_no,
             Toxer::err_str(err, Toxer::Context::Query));
    return QString();
  }

  auto out = new char[len];
  tox_friend_get_status_message(p_->tel_->tox_, friend_no, Toxer::to_tox(out), &err);
  if (Q_UNLIKELY(err)) {
    qWarning("Failed to get name of friend %d: %s", friend_no,
             Toxer::err_str(err, Toxer::Context::Query));
    // noreturn
  }
  QString outStr = QString::fromUtf8(out, static_cast<int>(len));
  delete[] out;
  return outStr;
}

/** @brief Returns the online status of a friend.
@param friend_no  the friend number
@returns the friend's online status; false if friend_no is invalid
*/
bool ToxProfile::is_online(quint32 friend_no) const
{
  QMutexLocker locker(&p_->tel_->mutex_);
  TOX_ERR_FRIEND_QUERY err;
  auto out = tox_friend_get_connection_status(p_->tel_->tox_, friend_no, &err);
  if (Q_UNLIKELY(err)) {
    qWarning("Failed to get online status of friend %d: %s", friend_no,
             Toxer::err_str(err, Toxer::Context::Query));
  }
  return out != TOX_CONNECTION_NONE;
}

/** @brief Returns the availability status of a friend.
@param friend_no  the friend number
@returns the friend's availability status
*/
ToxTypes::UserStatus ToxProfile::availability(quint32 friend_no) const
{
  QMutexLocker locker(&p_->tel_->mutex_);
  TOX_ERR_FRIEND_QUERY err;
  auto out = tox_friend_get_status(p_->tel_->tox_, friend_no, &err);
  if (err) {
    qWarning("Failed to get availability of friend %d: %s", friend_no,
             Toxer::err_str(err, Toxer::Context::Query));
  }
  return Toxer::fromTox(out);
}

/** @brief Returns the typing status of a friend.
@param friend_no  the friend number
@returns the friend's typing status; false if friend_no is invalid
*/
bool ToxProfile::is_typing(quint32 friend_no) const
{
  QMutexLocker locker(&p_->tel_->mutex_);
  TOX_ERR_FRIEND_QUERY err;
  bool out = tox_friend_get_typing(p_->tel_->tox_, friend_no, &err);
  if (Q_UNLIKELY(err)) {
    qWarning("Failed to get typing status of friend %d: %s", friend_no,
             Toxer::err_str(err, Toxer::Context::Query));
  }
  return out;
}

/** @brief Sets the Tox profile file notifier.
@note: the notifier can only be set once and there must only be one.
*/
void ToxProfile::setFileNotifier(IToxFileNotifier* notify)
{
  if (Q_LIKELY(notify != fileTransfer_.get())) {
    fileTransfer_.reset(notify);
  }
}

/** @brief Cancels a file transfer.
@param friend_no  the friend number
@param file_no  the file number
*/
void ToxProfile::cancelFileTransfer(quint32 friend_no, quint32 file_no)
{
  QMutexLocker locker(&p_->tel_->mutex_);
  tox_file_control(p_->tel_->tox_, friend_no, file_no, TOX_FILE_CONTROL_CANCEL, nullptr);
  // note: errors are ignored on cancel
  if (Q_LIKELY(fileTransfer_)) {
    fileTransfer_->on_cancelled(friend_no, file_no);
  }
}

/** @brief Pauses a file transfer.
@param friend_no  the friend number
@param file_no  the file number
*/
void ToxProfile::pauseFileTransfer(quint32 friend_no, quint32 file_no)
{
  QMutexLocker locker(&p_->tel_->mutex_);
  TOX_ERR_FILE_CONTROL err;
  tox_file_control(p_->tel_->tox_, friend_no, file_no, TOX_FILE_CONTROL_PAUSE, &err);
  if (err) {
    qWarning("Failed to pause file transfer: %s",
             Toxer::err_str(err, Toxer::Context::FileControl));
  } else if (Q_LIKELY(fileTransfer_)) {
    fileTransfer_->on_paused(friend_no, file_no);
  }
}

/** @brief Resumes a file transfer.
@param friend_no  the friend number
@param file_no  the file number
*/
void ToxProfile::resumeFileTransfer(quint32 friend_no, quint32 file_no)
{
  QMutexLocker locker(&p_->tel_->mutex_);
  TOX_ERR_FILE_CONTROL err;
  tox_file_control(p_->tel_->tox_, friend_no, file_no, TOX_FILE_CONTROL_RESUME, &err);
  if (err) {
    qWarning("Failed to resume file transfer: %s",
             Toxer::err_str(err, Toxer::Context::FileControl));
  } else if (Q_LIKELY(fileTransfer_)) {
    fileTransfer_->on_resumed(friend_no, file_no);
  }
}

/** @brief request sending a file to a Tox friend */
void ToxProfile::sendFile(uint32_t friend_no, const QString& path)
{
  if (!QFileInfo::exists(path)) {
    qWarning("Local file not found at %s", qUtf8Printable(path));
    return;
  }

  // send file transfer request
  Q_ASSERT(fileTransfer_);
  QMutexLocker locker(&p_->tel_->mutex_);
  QFileInfo info(path);
  auto name = info.fileName().toUtf8();
  TOX_ERR_FILE_SEND err;
  auto file_no = tox_file_send(p_->tel_->tox_, friend_no, TOX_FILE_KIND_DATA,
                               static_cast<uint64_t>(info.size()),
                               nullptr,
                               Toxer::to_tox(name.constData()),
                               static_cast<size_t>(name.size()),
                               &err);
  if (Q_UNLIKELY(err)) {
    qWarning("Failed to send file %s: %s", qUtf8Printable(path),
             Toxer::err_str(err, Toxer::Context::FileSend));
  } else {
    // notify on success
    fileTransfer_->on_file_transfer(friend_no, file_no, path);
  }
}

void ToxProfile::setUserName(const QString& userName) {
  TOX_ERR_SET_INFO err;
  toxSet([&](Tox* tox) {
    auto str = userName.toUtf8();
    tox_self_set_name(tox, Toxer::to_tox(str.constData()),
                      static_cast<size_t>(str.length()), &err);
  });
  if (Q_UNLIKELY(err)) {
    qWarning("Could not set user name: %s",
             Toxer::err_str(err, Toxer::Context::SetInfo));
  } else {
    persist();
    on_user_name_changed(userName);
  }
}

void ToxProfile::setStatusMessage(const QString& message) {
  TOX_ERR_SET_INFO err;
  toxSet([&](Tox* tox) {
    auto str = message.toUtf8();
    tox_self_set_status_message(tox, Toxer::to_tox(str.constData()),
                                static_cast<size_t>(str.length()), &err);
  });
  if (Q_UNLIKELY(err)) {
    qWarning("Could not set user name: %s",
             Toxer::err_str(err, Toxer::Context::SetInfo));
  } else {
    persist();
    on_status_message_changed(message);
  }
}

/** @brief Sets the Tox profile's user status. */
void ToxProfile::setUserStatus(ToxTypes::UserStatus status) {
  toxSet([status](Tox* tox) {
    tox_self_set_status(tox, Toxer::toTox(status));
  });
  on_status_changed(status);
}

/** @brief The Tox profile's user status changed.
@param status   the current user status
*/
void ToxProfile::on_status_changed(ToxTypes::UserStatus status)
{
  auto activeProfile = Toxer::activeProfile();
  Q_ASSERT(activeProfile);
  for (auto n : activeProfile->profileNotifiers_) {
    n->on_status_changed(status);
  }
}

/** @brief The Tox profile's user status message changed.
@param status   the current user status message
*/
void ToxProfile::on_status_message_changed(const QString& message)
{
  auto activeProfile = Toxer::activeProfile();
  Q_ASSERT(activeProfile);
  for (auto n : activeProfile->profileNotifiers_) {
    n->on_status_message_changed(message);
  }
}

/** @brief The Tox profile's user name changed.
@param status   the current user name
*/
void ToxProfile::on_user_name_changed(const QString& userName)
{
  auto activeProfile = Toxer::activeProfile();
  Q_ASSERT(activeProfile);
  for (auto n : activeProfile->profileNotifiers_) {
    n->on_user_name_changed(userName);
  }
}
