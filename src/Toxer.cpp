/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "Toxer.h"

#include "Private/ToxerPrivate.h"
#include "Private/ToxProfile.h"
#include "Settings.h"

#include <QDir>
#include <QFileInfo>

using Toxer::Api;
using Toxer::ToxAddressValidator;
using Toxer::ToxFriend;
using Toxer::ToxFriends;
using Toxer::ToxProfileQuery;

/**
@namespace Toxer

@class Api
@brief Provides a high-level Tox application management interface.

By design only single instance of Toxer is required within a QApplication or
QGuiApplication.


@class ToxAddressValidator
@brief Validator class for Tox address strings.


@class ToxProfileQuery
@brief Query object to get and set Tox profile information.

If the Tox profile is modified, the query object also notifies via Qt signal.

@fn userNameChanged
@brief The Tox profile's user name changed.
@param[in] userName     the new user name

@fn isOnlineChanged
@brief The Tox profile's user online status changed.
@param[in] online   the new online status

@fn statusMessageChanged
@brief The Tox profile's user status message changed.
@param[in] statusMessage    the new status message

@fn statusChanged
@brief The Tox profile's user availability changed.
@param[in] status   the new user's availability status

@fn friendRequest
@brief A friend request was received.
@param[in] pk   the Tox public key of person requesting Tox friendship
@param[in] message  a message requesting to add the new friend

@fn persist
@brief save the profile to the profile file 

@class ToxFriends
@brief Qt abstraction of Tox friend management.

Notifies about changes in Tox friend specific information. Further provides
access to friend management.

@fn countChanged
@brief Signals a change in the friendlist count by adding or removing a friend.

@fn added
@brief Signals a friend has been added.
@param[in] index    the index of the added friend

@fn removed
@brief Signals removal of a friend.
@param[in] index    the previous index of the removed friend

@fn nameChanged
@brief A friend's name was changed..
@param[in] index    the index of the friend
@param[in] name     the friend's new name

@fn statusMessageChanged
@brief Signals a change of a friend's status message.
@param[in] index    the index of the friend
@param[in] statusMessage    the friend's new status message

@fn statusChanged
@brief Signals a change of a friend's availability status.
@param[in] index    the index of the friend
@param[in] name     the friend's new name

@fn isOnlineChanged
@brief Signals a change of a friend's online status.
@param[in] index    the index of the friend
@param[in] online     the friend's new online status

@fn isTypingChanged
@brief Signals that a friend started/stopped typing.
@param[in] index    the index of the friend
@param[in] name     the friend's typing status

@fn message
@brief Signals an incoming message from a  friend.
@param[in] index    the index of the friend
@param[in] name     the friend's message
*/

/** @class ToxerApi::FileTransfer
@brief Toxer file transfer
*/
class Api::FileTransfer : public IToxFileNotifier
{
  using FileKey = QPair<quint32, quint32>;
  using FileLocator = QMap<FileKey, QString>;
public:
  FileTransfer(Api& t) : t_(t) {}
  ~FileTransfer() final; // out-of-line destructor

private: // IToxFileNotifier interface
  /** @brief A Tox friend requests to send the avatar.
  @param friend_no  the friend number
  @param file_no  the file number
  @param file_size  the avatar file size
  @note Avatar is rejected if file size exceeds 512KiB.
  */
  void on_avatar_request(quint32 friend_no, quint32 file_no, quint64 file_size) final {
    constexpr auto max_file_size = 1024 * 512; // 512KiB max.
    if (file_size <= max_file_size) {
      QMetaObject::invokeMethod(&t_, "avatarRequest", Qt::QueuedConnection,
                                Q_ARG(int, static_cast<int>(friend_no)),
								Q_ARG(int, static_cast<int>(file_no)));
                                //Q_ARG(int, static_cast<int>(file_no)),
                                //Q_ARG(quint64, file_size));
    } else {
      qInfo("Avatar exceeds maximum size of %d bytes. -> rejecting", max_file_size);
      auto p = Toxer::activeProfile();
      p->cancelFileTransfer(friend_no, file_no);
    }
  }

  /** @brief A file transfer has been cancelled or rejected. */
  void on_cancelled(quint32 friend_no, quint32 file_no) final {
    QMetaObject::invokeMethod(&t_, "fileCancelled", Qt::QueuedConnection,
                              Q_ARG(int, static_cast<int>(friend_no)),
                              Q_ARG(int, static_cast<int>(file_no)));
  }

  /** @brief A Tox friend requests to send a file. */
  void on_file_request(quint32 friend_no, quint32 file_no, const QString &name, quint64 file_size) final {
    QMetaObject::invokeMethod(&t_, "fileRequest", Qt::QueuedConnection,
                              Q_ARG(int, static_cast<int>(friend_no)),
                              Q_ARG(int, static_cast<int>(file_no)),
                              Q_ARG(QString, name),
                              Q_ARG(quint64, file_size));
  }

  /** @brief A new file transfer has been initiated. */
  void on_file_transfer(quint32 friend_no, quint32 file_no, const QString& path) final {
    f_loc_.insert(FileKey(friend_no, file_no), path);
    // TODO: emit "changed" signal refreshing the view
  }

  /** @brief received chunk of file in transfer */
  void on_chunk(quint32 friend_no, quint32 file_no, quint64 pos, const char* data, quint64 len) final {
    // TODO: write data to file at given position
    Q_UNUSED(pos)
    QString path = locate_file(friend_no, file_no);
    if (!path.isEmpty()) {
      QFile f(path);
      if (f.open(QFile::WriteOnly|QFile::Append)) {
        auto c_len = static_cast<int>(len);
        f.write(data, c_len);
        QMetaObject::invokeMethod(&t_, "fileChunk", Qt::QueuedConnection,
                                  Q_ARG(int, static_cast<int>(friend_no)),
                                  Q_ARG(int, static_cast<int>(file_no)),
                                  Q_ARG(quint64, len));
      }
    }
  }

  /** @brief send next chunk */
  void on_next_chunk(quint32 friend_no, quint32 file_no, quint64 pos, quint64 len) {
    auto p = Toxer::activeProfile(); Q_ASSERT(p);
    QString path = locate_file(friend_no, file_no);
    QFile f(path);
    if (!f.open(QFile::ReadOnly)) {
      p->cancelFileTransfer(friend_no, file_no);
      return;
    }
    if (!f.seek(static_cast<qint64>(pos))) {
      p->cancelFileTransfer(friend_no, file_no);
      return;
    }
    QByteArray data = f.read(static_cast<qint64>(len));
    p->toxSet([data, friend_no, file_no, pos](Tox* tox){
      auto c_data = reinterpret_cast<const quint8*>(data.constData());
      auto c_bytes_read = static_cast<size_t>(data.size());
      TOX_ERR_FILE_SEND_CHUNK err;
      tox_file_send_chunk(tox, friend_no, file_no, pos, c_data, c_bytes_read, &err);
    });
  }

  /** @brief file transfer completed */
  void on_finished(quint32 friend_no, quint32 file_no) final {
    QMetaObject::invokeMethod(&t_, "fileFinished", Qt::QueuedConnection,
                              Q_ARG(int, static_cast<int>(friend_no)),
                              Q_ARG(int, static_cast<int>(file_no)));
  }

  /** @brief file transfer paused */
  void on_paused(quint32 friend_no, quint32 file_no) final {
    QMetaObject::invokeMethod(&t_, "filePaused", Qt::QueuedConnection,
                              Q_ARG(int, static_cast<int>(friend_no)),
                              Q_ARG(int, static_cast<int>(file_no)));
  }

  /** @brief file_transfer resumed */
  void on_resumed(quint32 friend_no, quint32 file_no) final {
    QMetaObject::invokeMethod(&t_, "fileResumed", Qt::QueuedConnection,
                              Q_ARG(int, static_cast<int>(friend_no)),
                              Q_ARG(int, static_cast<int>(file_no)));
  }

  /** @brief locate a file on disk */
  QString locate_file(quint32 friend_no, quint32 file_no) {
    FileKey fid(friend_no, file_no);
    return f_loc_[fid];
  }

private:
  Api& t_;
  FileLocator f_loc_;
};
Api::FileTransfer::~FileTransfer() { /* out-of-line virtual destructor */}

/** @brief The Tox library version string. */
QString Api::toxVersionString() {
  return QString::fromLatin1("%1.%2.%3")
      .arg(TOX_VERSION_MAJOR)
      .arg(TOX_VERSION_MINOR)
      .arg(TOX_VERSION_PATCH);
}

/** @brief Returns the system path where Tox profiles live.
@return the path to Tox profiles location
*/
QString Api::profileLocation() const { return Toxer::profilesDir(); }

/** @brief Returns a string list of available Tox profiles.
@return the list of available profile names

Searches for Tox profiles (*.tox files). The lookup location depends on the
target system.
*/
QStringList Api::availableProfiles() const {
  QDir dir(Toxer::profilesDir());
  dir.setFilter(QDir::Files | QDir::NoDotAndDotDot);
  dir.setNameFilters(QStringList() << QStringLiteral("*.tox"));
  auto list = dir.entryInfoList();
  QStringList out;
  for (QFileInfo info : list) {
    out << info.completeBaseName();
  }
  return out;
}

/** @brief Activates a Tox profile.
@param[in] profileName  the profile name
*/
bool Api::activateProfile(const QString& profileName, const QString& password)
{
  const auto old = Toxer::activeProfile();
  bool result = Toxer::activate(profileName, password);
  if(!result) return false;
  
  auto p = Toxer::activeProfile();
  if (old != p) {
    p->setFileNotifier(new FileTransfer(*this));
    emit profileChanged();
  }
  return true;
}

/** @brief Creates a Tox profile.
@param profileName  the profile name
@param password     the profile password
*/
bool Api::createProfile(const QString& profileName, const QString& password,
                          const QString& toxAlias)
{
  ToxProfile::create(profileName, password, toxAlias);
}

/** @brief Closes the current Tox profile */
void Api::closeProfile() {
  auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    delete p;
    emit profileChanged();
  }
}

/** @brief Returns, if Toxer has loaded a profile.
@return true when a profile is loaded; false otherwise
*/
bool Api::hasProfile() const {
  return Toxer::activeProfile() != nullptr;
}

/** @brief Returns the URL to the local avatars directory.
@returns the local url to avatars directory
*/
QUrl Api::avatarsUrl() const {
  return QUrl::fromLocalFile(Toxer::profilesDir() + QStringLiteral("/avatars"));
}

/** @brief Checks, if a URL points to an existing local file or directory.
@param url     the URL to check
@returns true, if file or directory exists; false otherwise
*/
bool Api::exists(const QUrl& url) const
{
  return QFileInfo::exists(url.toLocalFile());
}

/** @brief ToxProfileQuery constructor
@param[in] parent
*/
ToxProfileQuery::ToxProfileQuery(QObject* parent)
  : QObject(parent)
{
}

/** @brief profile user name changed notifier
@param userName     the user name
*/
void ToxProfileQuery::on_user_name_changed(const QString& userName)
{
  QMetaObject::invokeMethod(this, "userNameChanged", Qt::QueuedConnection,
                            Q_ARG(QString, userName));
}

/** @brief profile is online changed notifier
@param online   the online status
*/
void ToxProfileQuery::on_is_online_changed(bool online)
{
  QMetaObject::invokeMethod(this, "isOnlineChanged", Qt::QueuedConnection,
                            Q_ARG(bool, online));
}

/** @brief profile status message changed notifier
@param message  the status message
*/
void ToxProfileQuery::on_status_message_changed(const QString& message)
{
  QMetaObject::invokeMethod(this, "statusMessageChanged", Qt::QueuedConnection,
                            Q_ARG(QString, message));
}

/** @brief profile status changed notifier
@param status   the user status
*/
void ToxProfileQuery::on_status_changed(ToxTypes::UserStatus status)
{
  QMetaObject::invokeMethod(this, "statusChanged", Qt::QueuedConnection,
                            Q_ARG(quint8, static_cast<quint8>(status)));
}

/** @brief A Tox friend request has been received.
@param pk   the (potential) friend's public key
@param msg  invitation message from the potential friend
*/
void ToxProfileQuery::on_friend_request(const QByteArray& pk, const QString& msg)
{
  QMetaObject::invokeMethod(this, "friendRequest", Qt::QueuedConnection,
                            Q_ARG(QString, QString::fromLatin1(pk.toHex())),
                            Q_ARG(QString, msg));
}

/** @brief Returns the profile name.
@return the valid profile name or an empty string

The profile name represents the base name of a '.tox' file.
*/
QString ToxProfileQuery::name() const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->name() : QString();
}

/** @brief Returns the local profile's user name.
@return the user name
*/
QString ToxProfileQuery::userName() const
{
  QString outStr;
  auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    p->toxQuery([&outStr](const Tox* tox) {
      auto len = tox_self_get_name_size(tox);
      auto out = new char[len];
      tox_self_get_name(tox, reinterpret_cast<uint8_t*>(out));
      outStr = QString::fromUtf8(out, static_cast<int>(len));
      delete[] out;
    });
  }
  return outStr;
}

/** @brief Sets the local profile'S user name.
@param newValue     the user name
*/
void ToxProfileQuery::setUserName(const QString& newValue)
{
  auto p = Toxer::activeProfile();
  QString oldValue = userName();
  if (Q_LIKELY(p && newValue != oldValue)) {
    p->setUserName(newValue);
  }
}

/** @brief Returns the local profile's user status message.
@return the status message
*/
QString ToxProfileQuery::statusMessage() const
{
  QString outStr;
  auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    p->toxQuery([&outStr](const Tox* tox) {
      auto len = tox_self_get_status_message_size(tox);
      auto out = new char[len];
      tox_self_get_status_message(tox, reinterpret_cast<uint8_t*>(out));
      outStr = QString::fromUtf8(out, static_cast<int>(len));
      delete[] out;
    });
  }
  return outStr;
}

/** @brief Sets the local profile's user status message.
@param newValue     the status message
*/
void ToxProfileQuery::setStatusMessage(const QString& newValue)
{
  auto p = Toxer::activeProfile();
  QString oldValue = userName();
  if (p && newValue != oldValue) {
    p->setStatusMessage(newValue);
  }
}

/** @brief Returns the local profile's Tox address.
@return the Tox address (pk + nospam + bcc16)
*/
QString ToxProfileQuery::addressStr() const
{
  QString outStr;
  auto p = Toxer::activeProfile();
  if (p) {
    p->toxQuery([&outStr](const Tox* tox) {
      uint8_t addr[Toxer::address_size()];
      tox_self_get_address(tox, addr);

      // convert to hex-string
      auto addr_raw = QByteArray::fromRawData(
            reinterpret_cast<const char*>(addr),
            Toxer::address_size());
      outStr = QString::fromLatin1(addr_raw.toHex());
    });
  }

  return outStr;
}

/** @brief Returns the local profile's user public key string.
@return the public key as a null-terminated hex-string
*/
QString ToxProfileQuery::publicKeyStr() const
{
  QString outStr;
  auto p = Toxer::activeProfile();
  if (p) {
    p->toxQuery([&outStr](const Tox* tox) {
      uint8_t pk[Toxer::public_key_size()];
      tox_self_get_public_key(tox, pk);

      // convert to hex-string
      auto pk_raw = QByteArray::fromRawData(
            reinterpret_cast<const char*>(pk),
            Toxer::public_key_size());
      outStr = QString::fromLatin1(pk_raw.toHex());
    });
  }

  return outStr;
}

/** @brief Returns the local profile's user nospam value.
@return the nospam value
*/
quint32 ToxProfileQuery::nospam() const
{
  quint32 out = 0;
  auto p = Toxer::activeProfile();
  if (p) {
    p->toxQuery([&out](const Tox* tox) {
      out = tox_self_get_nospam(tox);
    });
  }

  return out;
}

/** @brief Checks, if a public key matches the profile key.
@param pk
@return
*/
bool ToxProfileQuery::isMe(QString pk) const
{
  return pk == publicKeyStr();
}

/** @brief Returns the local profile's online status.
@return true if online; false otherwise
*/
bool ToxProfileQuery::isOnline() const
{
  bool out = false;
  auto p = Toxer::activeProfile();
  if (p) {
    p->toxQuery([&out](const Tox* tox) {
      TOX_CONNECTION status = tox_self_get_connection_status(tox);
      out = status != TOX_CONNECTION_NONE;
    });
  }

  return out;
}

/** @brief Returns the local profile's user status.
@return the user status
*/
ToxTypes::UserStatus ToxProfileQuery::status() const
{
  auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    ToxTypes::UserStatus out;
    p->toxQuery([&out](const Tox* tox) {
      auto status = tox_self_get_status(tox);
      out = Toxer::fromTox(status);
    });
    return out;
  } else {
    return ToxTypes::UserStatus::Away;
  }
}

/** @brief Returns the local profile's user status as an integer.
@return the user status
*/
quint8 ToxProfileQuery::statusInt() const
{
  return static_cast<quint8>(status());
}

/** @brief Sets the local profile'S user status
@param status   the user status
*/
void ToxProfileQuery::setStatus(quint8 newValue)
{
  auto p = Toxer::activeProfile();
  auto new_v = static_cast<ToxTypes::UserStatus>(newValue);
  auto old_v = status();
  if (Q_LIKELY(p && new_v != old_v)) {
    p->setUserStatus(new_v);
  }
}

/** @brief Invite a Tox friend to connect.
@param[in] toxId   the friend's Tox address (pk + nospam + chk)
@param[in] message  the message is shown to the friend

The friend is added to the current profile.
*/
bool ToxProfileQuery::inviteFriend(const QString& toxId, const QString& message)
{
  auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    return p->inviteFriend(toxId, message);
  }
  return false;
}

void ToxProfileQuery::persist()
{
	auto p = Toxer::activeProfile();
	if(Q_LIKELY(p)){
		p->persist();
	}
}

/** @brief ToxFriend constructor */
ToxFriend::ToxFriend(QObject* parent)
  : QObject(parent)
{
}

/** @brief Returns the public key of the friend. */
QString ToxFriend::pkStr() const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->pk_str(static_cast<quint32>(fno_)) : QString();
}

/** @brief the Tox friend's name
@return the friend's name
*/
QString ToxFriend::name() const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->name(static_cast<quint32>(fno_)) : QString();
}

/** @brief the Tox friend's status message
@return the (optional) status message or an empty string
*/
QString ToxFriend::statusMessage() const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->status_message(static_cast<quint32>(fno_)) : QString();
}

/** @brief the Tox friend's online status
@return true when friend is online; false otherwise
*/
bool ToxFriend::isOnline() const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->is_online(static_cast<quint32>(fno_)) : false;
}

/** @brief the Tox friend's availability status
@return a ToxTypes::UserStatus enumeration
*/
ToxTypes::UserStatus ToxFriend::availability() const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->availability(static_cast<quint32>(fno_)) : ToxTypes::UserStatus::Away;
}

/** @brief the Tox friend's availability status
@return integer representation of ToxTypes::UserStatus enumeration
*/
quint8 ToxFriend::availabilityInt() const
{
  return static_cast<quint8>(availability());
}

/** @brief the Tox friend's typing status
@return true when friend is typing; false otherwise
*/
bool ToxFriend::isTyping() const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->is_typing(static_cast<quint32>(fno_)) : false;
}

/** @brief Sends message to the known friend or group.
@param message  the message
*/
bool ToxFriend::sendMessage(const QString& message)
{
  auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    return p->send_message(static_cast<quint32>(fno_), message);
  }
  return false;
}

/** @brief request a friend to accept a file
@param friendNo   the friend number
@param url  the local file url
*/
void ToxFriend::sendFile(QUrl url) {
  auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    p->sendFile(static_cast<quint32>(fno_), url.path());
  }
}

/** @reimp */
void ToxFriend::on_name_changed(quint32 friend_no, const QString& name)
{
  if (friend_no == static_cast<quint32>(fno_)) {
    QMetaObject::invokeMethod(this, "nameChanged", Qt::QueuedConnection,
                              Q_ARG(QString, name));
  }
}

/** @reimp */
void ToxFriend::on_status_message_changed(quint32 friend_no, const QString& message)
{
  if (friend_no == static_cast<quint32>(fno_)) {
    QMetaObject::invokeMethod(this, "statusMessageChanged", Qt::QueuedConnection,
                              Q_ARG(QString, message));
  }
}

/** @brief friend status changed notifier
@param friend_no  the friend number
*/
void ToxFriend::on_status_changed(quint32 friend_no, ToxTypes::UserStatus status)
{
  if (friend_no == static_cast<quint32>(fno_)) {
    QMetaObject::invokeMethod(this, "availabilityChanged", Qt::QueuedConnection,
                              Q_ARG(quint8, static_cast<quint8>(status)));
  }
}

/** @brief friend is online changed notifier
@param friend_no  the friend number
*/
void ToxFriend::on_is_online_changed(quint32 friend_no, bool online)
{
  if (friend_no == static_cast<quint32>(fno_)) {
    QMetaObject::invokeMethod(this, "isOnlineChanged", Qt::QueuedConnection,
                              Q_ARG(bool, online));
  }
}

/** @brief friend is typing notifier
@param friend_no  the friend number
*/
void ToxFriend::on_is_typing_changed(quint32 friend_no, bool typing)
{
  if (friend_no == static_cast<quint32>(fno_)) {
    QMetaObject::invokeMethod(this, "isTypingChanged", Qt::QueuedConnection,
                              Q_ARG(bool, typing));
  }
}

/** @brief friend message received notifier
@param friend_no  the friend number
*/
void ToxFriend::on_message(quint32 friend_no, const QString& message)
{
  if (friend_no == static_cast<quint32>(fno_)) {
    QMetaObject::invokeMethod(this, "message", Qt::QueuedConnection,
                              Q_ARG(QString, message));
  }
}

/** @brief ToxFriends constructor */
ToxFriends::ToxFriends(QObject* parent)
  : QObject(parent)
{
}

/** @brief Returns the number of friends for the active profile. */
int ToxFriends::count() const
{
  const auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    size_t out = 0;
    p->toxQuery([&out](const Tox* tox){
      out = tox_self_get_friend_list_size(tox);
    });
    return static_cast<int>(out);
  }

  return 0;
}

/** @brief Returns a list of friend id's for the active profile. */
QList<int> ToxFriends::list() const
{
  QList<int> out;
  const auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    p->toxQuery([&out](const Tox* tox) {
      const size_t cnt = tox_self_get_friend_list_size(tox);
      quint32* ids = new quint32[cnt];
      tox_self_get_friend_list(tox, ids);
      for (size_t i = 0; i < cnt; ++i) {
        out << static_cast<int>(ids[i]);
      }
      delete[] ids;
    });
  }

  return out;
}

/** @brief Remove a Tox friend from the profile.
@param friendNo  the friend number
*/
void ToxFriends::remove(int friendNo)
{
  auto p = Toxer::activeProfile();
  if (Q_LIKELY(p)) {
    p->deleteFriend(static_cast<quint32>(friendNo));
  }
}

/** @brief Returns the public key of a friend.
@param friendNo   the friend number
*/
QString ToxFriends::publicKeyStr(int friendNo) const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->pk_str(static_cast<uint32_t>(friendNo)) : QString();
}

/** @brief Returns the name of a friend.
@param friendNo  the friend number
@returns the friend's name; empty if index is invalid
*/
QString ToxFriends::name(int friendNo) const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->name(static_cast<quint32>(friendNo)) : QString();
}

/** @brief Returns the status message of a friend.
@param friendNo  the friend number
@returns the friend's status message; empty if index is invalid
*/
QString ToxFriends::statusMessage(int friendNo) const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->status_message(static_cast<quint32>(friendNo)) : QString();
}

/** @brief Returns the online status of a friend.
@param friendNo  the friend number
@return the online status
*/
bool ToxFriends::isOnline(int friendNo) const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->is_online(static_cast<quint32>(friendNo)) : false;
}

/** @brief Returns the friend's current user status.
@param friendNo  the friend number
@return the current user status
*/
ToxTypes::UserStatus ToxFriends::status(int friendNo) const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->availability(static_cast<quint32>(friendNo))
                     : ToxTypes::UserStatus::Away;
}

/** @brief Returns the Tox status
@param friendNo  the friend number
@return the currrent user status as integer
*/
quint8 ToxFriends::statusInt(int friendNo) const
{
  return static_cast<quint8>(status(friendNo));
}

/** @brief Returns the typing status.
@param friendNo  the friend number
@return true, if the friend is typing; false otherwise
@note The typing status is only transferred, if the friend enables it.
*/
bool ToxFriends::isTyping(int friendNo) const
{
  auto p = Toxer::activeProfile();
  return Q_LIKELY(p) ? p->is_typing(static_cast<quint32>(friendNo)) : false;
}

/** @brief Cancels a file transfer that is in progress.
@param fileNo   the file number
*/
void ToxFriend::cancelFile(int fileNo)
{
  auto p = Toxer::activeProfile(); Q_ASSERT(p);
  p->cancelFileTransfer(static_cast<quint32>(fno_), static_cast<quint32>(fileNo));
}

/** @brief Pauses a file transfer that is in progress.
@param fileNo   the file number
*/
void ToxFriend::pauseFile(int fileNo)
{
  auto p = Toxer::activeProfile(); Q_ASSERT(p);
  p->pauseFileTransfer(static_cast<quint32>(fno_), static_cast<quint32>(fileNo));
}

/** @brief Resumes a file transfer that is in progress.
@param fileNo   the file number
*/
void ToxFriend::resumeFile(int fileNo)
{
  auto p = Toxer::activeProfile(); Q_ASSERT(p);
  p->pauseFileTransfer(static_cast<quint32>(fno_), static_cast<quint32>(fileNo));
}

/** @brief friend added notifier
@param friend_no  the friend number
*/
void ToxFriends::on_added(quint32 friend_no)
{
  QMetaObject::invokeMethod(this, "added", Qt::QueuedConnection,
                            Q_ARG(int, static_cast<int>(friend_no)));
  emit_count_changed();
}

/** @brief friend deleted notifier
@param friend_no  the friend number
*/
void ToxFriends::on_deleted(quint32 friend_no)
{
  QMetaObject::invokeMethod(this, "removed", Qt::QueuedConnection,
                            Q_ARG(int, static_cast<int>(friend_no)));
  emit_count_changed();
}

/** @brief friend name changed notifier
@param friend_no  the friend number
*/
void ToxFriends::on_name_changed(quint32 friend_no, const QString& name)
{
  QMetaObject::invokeMethod(this, "nameChanged", Qt::QueuedConnection,
                            Q_ARG(int, static_cast<int>(friend_no)),
                            Q_ARG(QString, name));
}

/** @brief friend status message changed notifier
@param friend_no  the friend number
*/
void ToxFriends::on_status_message_changed(quint32 friend_no, const QString& message)
{
  QMetaObject::invokeMethod(this, "statusMessageChanged", Qt::QueuedConnection,
                            Q_ARG(int, static_cast<int>(friend_no)),
                            Q_ARG(QString, message));
}

/** @brief friend status changed notifier
@param friend_no  the friend number
*/
void ToxFriends::on_status_changed(quint32 friend_no, ToxTypes::UserStatus status)
{
  QMetaObject::invokeMethod(this, "statusChanged", Qt::QueuedConnection,
                            Q_ARG(int, static_cast<int>(friend_no)),
                            Q_ARG(quint8, static_cast<quint8>(status)));
}

/** @brief friend is online changed notifier
@param friend_no  the friend number
*/
void ToxFriends::on_is_online_changed(quint32 friend_no, bool online)
{
  QMetaObject::invokeMethod(this, "isOnlineChanged", Qt::QueuedConnection,
                            Q_ARG(int, static_cast<int>(friend_no)),
                            Q_ARG(bool, online));
}

/** @brief friend is typing notifier
@param friend_no  the friend number
*/
void ToxFriends::on_is_typing_changed(quint32 friend_no, bool typing)
{
  QMetaObject::invokeMethod(this, "isTypingChanged", Qt::QueuedConnection,
                            Q_ARG(int, static_cast<int>(friend_no)),
                            Q_ARG(bool, typing));
}

/** @brief friend message received notifier
@param friend_no  the friend number
*/
void ToxFriends::on_message(quint32 friend_no, const QString& message)
{
  QMetaObject::invokeMethod(this, "message", Qt::QueuedConnection,
                            Q_ARG(int, static_cast<int>(friend_no)),
                            Q_ARG(QString, message));
}

/** @brief constructor */
ToxAddressValidator::ToxAddressValidator(QObject* parent)
  : QValidator(parent)
{
}

/** @brief Validates Tox address strings.
@param str  the string to validate
@return The QValidator state.
@reimp
*/
QValidator::State ToxAddressValidator::validate(QString& input, int& pos) const
{
  Q_UNUSED(pos)
  constexpr auto tox_addr_hex_len = 2 * Toxer::address_size();
  if (input.size() > tox_addr_hex_len) {
    return State::Invalid;
  }

  // note: State::Intermediate ensures that the string is always editable
  return isToxAddress(input) ? State::Acceptable : State::Intermediate;
}

/** @brief Checks if a string is a Tox address.
@param str   the string to check
@return true when str represents a valid Tox address
*/
bool ToxAddressValidator::isToxAddress(const QString& str) const {
  const auto tox_addr = QByteArray::fromHex(str.toLatin1().toLower());
  if (tox_addr.size() != Toxer::address_size()) {
    // hex-string conversion failed
    return false;
  }

  return Toxer::check_address(tox_addr);
}
