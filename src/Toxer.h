/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#include "ToxTypes.h"
#include "IToxNotify.h"

#include <QUrl>
#include <QValidator>

namespace Toxer {

class Api : public QObject
{
  Q_OBJECT
  class FileTransfer;

public:
  Api() = default;

  Q_INVOKABLE QString toxVersionString();
  Q_INVOKABLE QString profileLocation() const;
  Q_INVOKABLE QStringList availableProfiles() const;
  Q_INVOKABLE bool activateProfile(const QString& profileName,
                                   const QString& password);
  Q_INVOKABLE bool createProfile(const QString& profileName,
                                 const QString& password,
                                 const QString& toxAlias);
  Q_INVOKABLE void closeProfile();
  Q_INVOKABLE bool hasProfile() const;
  Q_INVOKABLE QUrl avatarsUrl() const;
  Q_INVOKABLE bool exists(const QUrl& url) const;

signals:
  void avatarRequest(int friendNo, int fileNo);
  void fileCancelled(int friendNo, int fileNo);
  void fileChunk(int friendNo, int fileNo, int size);
  void fileFinished(int friendNo, int fileNo);
  void filePaused(int friendNo, int fileNo);
  void fileResumed(int friendNo, int fileNo);
  void fileRequest(int friendNo, int fileNo, const QString& name, quint64 size);
  void profileChanged();
};

class ToxAddressValidator : public QValidator
{
  Q_OBJECT
public:
  ToxAddressValidator(QObject* parent=nullptr);

  State validate(QString& input, int& pos) const override;

  Q_INVOKABLE bool isToxAddress(const QString& str) const;
};

class ToxProfileQuery : public QObject, IToxProfileNotifier
{
  Q_OBJECT
public:
  ToxProfileQuery(QObject* parent = nullptr);

public:
  Q_INVOKABLE QString name() const;
  Q_INVOKABLE QString userName() const;
  Q_INVOKABLE void setUserName(const QString& newValue);
  Q_INVOKABLE QString statusMessage() const;
  Q_INVOKABLE void setStatusMessage(const QString& newValue);
  Q_INVOKABLE QString addressStr() const;
  Q_INVOKABLE QString publicKeyStr() const;
  Q_INVOKABLE quint32 nospam() const;
  Q_INVOKABLE bool isMe(QString pk) const;
  Q_INVOKABLE bool isOnline() const;
  ToxTypes::UserStatus status() const;
  Q_INVOKABLE quint8 statusInt() const;
  Q_INVOKABLE void setStatus(quint8 newValue);
  Q_INVOKABLE bool inviteFriend(const QString& toxId, const QString& message);
  Q_INVOKABLE void persist();
  
signals:
  void userNameChanged(const QString& userName);
  void isOnlineChanged(bool online);
  void statusMessageChanged(const QString& statusMessage);
  void statusChanged(int status);
  void friendRequest(const QString& pk, const QString& message);

private:
  // IToxProfileNotifier interface
  void on_user_name_changed(const QString& userName) override;
  void on_is_online_changed(bool online) override;
  void on_status_message_changed(const QString& message) override;
  void on_status_changed(ToxTypes::UserStatus status) override;
  void on_friend_request(const QByteArray& pk, const QString& msg) override;
};

class ToxFriend : public QObject, IToxFriendNotifier
{
  Q_OBJECT
  Q_PROPERTY(quint8 availability READ availabilityInt NOTIFY availabilityChanged)
  Q_PROPERTY(int fno MEMBER fno_)
  Q_PROPERTY(bool isOnline READ isOnline NOTIFY isOnlineChanged)
  Q_PROPERTY(bool isTyping READ isTyping NOTIFY isTypingChanged)
  Q_PROPERTY(QString name READ name NOTIFY nameChanged)
  Q_PROPERTY(QString statusMessage READ statusMessage NOTIFY statusMessageChanged)

public:
  ToxFriend(QObject* parent=nullptr);

  Q_INVOKABLE QString pkStr() const;
  Q_INVOKABLE QString name() const;
  Q_INVOKABLE QString statusMessage() const;
  Q_INVOKABLE bool isOnline() const;
  ToxTypes::UserStatus availability() const;
  Q_INVOKABLE quint8 availabilityInt() const;
  Q_INVOKABLE bool isTyping() const;

  Q_INVOKABLE bool sendMessage(const QString& text);

  // file transfer
  Q_INVOKABLE void cancelFile(int fileNo);
  Q_INVOKABLE void pauseFile(int fileNo);
  Q_INVOKABLE void resumeFile(int fileNo);
  Q_INVOKABLE void sendFile(QUrl url);

signals:
  void nameChanged(const QString& name);
  void statusMessageChanged(const QString& statusMessage);
  void availabilityChanged(quint8 status);
  void isOnlineChanged(bool online);
  void isTypingChanged(bool typing);
  void message(const QString& message);

private:
  // IToxFriendNotifier interface
  void on_name_changed(quint32 friend_no, const QString& name) override;
  void on_status_message_changed(quint32 friend_no, const QString& message) override;
  void on_status_changed(quint32 friend_no, ToxTypes::UserStatus status) override;
  void on_is_online_changed(quint32 friend_no, bool online) override;
  void on_is_typing_changed(quint32 friend_no, bool typing) override;
  void on_message(quint32 friend_no, const QString& message) override;

protected:
  int fno_ = -1;
};

class ToxFriends : public QObject, IToxFriendsNotifier, IToxFriendNotifier
{
  Q_OBJECT
  Q_PROPERTY (int count
              READ count
              NOTIFY countChanged)
  Q_PROPERTY(QList<int> list
             READ list
             NOTIFY countChanged)
public:
  ToxFriends(QObject* parent = nullptr);

public:
  int count() const;
  QList<int> list() const;

  Q_INVOKABLE void remove(int friendNo);
  Q_INVOKABLE QString publicKeyStr(int friendNo) const;
  Q_INVOKABLE QString name(int friendNo) const;
  Q_INVOKABLE QString statusMessage(int friendNo) const;
  Q_INVOKABLE bool isOnline(int friendNo) const;
  ToxTypes::UserStatus status(int friendNo) const;
  Q_INVOKABLE quint8 statusInt(int friendNo) const;
  Q_INVOKABLE bool isTyping(int friendNo) const;

signals:
  void countChanged();
  void added(int index);
  void removed(int index);
  void nameChanged(int index, const QString& name);
  void statusMessageChanged(int index, const QString& statusMessage);
  void statusChanged(int index, quint8 status);
  void isOnlineChanged(int index, bool online);
  void isTypingChanged(int index, bool typing);
  void message(int index, const QString& message);

private:
  inline void emit_count_changed() {
    QMetaObject::invokeMethod(this, "countChanged", Qt::QueuedConnection);
  }

  // IToxFriendsNotifier interface
  void on_added(quint32 friend_no) override;
  void on_deleted(quint32 friend_no) override;

  // IToxFriendNotifier interface
  void on_name_changed(quint32 friend_no, const QString& name) override;
  void on_status_message_changed(quint32 friend_no, const QString& message) override;
  void on_status_changed(quint32 friend_no, ToxTypes::UserStatus status) override;
  void on_is_online_changed(quint32 friend_no, bool online) override;
  void on_is_typing_changed(quint32 friend_no, bool typing) override;
  void on_message(quint32 friend_no, const QString& message) override;
};

} // namespace Toxer
