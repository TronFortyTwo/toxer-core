/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2017-2019 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

#include "ToxTypes.h"

#include <QSettings>
#include <QRect>
#include <QVector>

namespace Toxer {

class Settings {
protected:
  using Scope = QSettings::Scope;
  Settings(Scope scope, const QString& filename = QStringLiteral("settings"));

  inline bool set(const QString& key, const QVariant& v, const QVariant& v_old) {
    if (v == v_old) {
      // unchanged
      return false;
    } else {
      s_.setValue(key, v);
      return true;
    }
  }

protected:
  QSettings s_;
};

class ToxSettings : public QObject, Settings {
  Q_OBJECT
  Q_PROPERTY(bool ip6 READ ip6 WRITE setIp6 NOTIFY changed FINAL)
  Q_PROPERTY(bool udp READ udp WRITE setUdp NOTIFY changed FINAL)
  Q_PROPERTY(ToxTypes::Proxy proxyType READ proxyType WRITE setProxyType NOTIFY changed FINAL)
  Q_PROPERTY(quint16 proxyPort READ proxyPort WRITE setProxyPort NOTIFY changed FINAL)
  Q_PROPERTY(QString proxyAddress READ proxyAddress WRITE setProxyAddress NOTIFY changed FINAL)
public:
  ToxSettings(Scope scope = Scope::UserScope, QObject* parent = nullptr);
  
  Q_INVOKABLE bool ip6() const;
  Q_INVOKABLE void setIp6(bool enabled);
  Q_INVOKABLE void resetIp6();

  Q_INVOKABLE bool udp() const;
  Q_INVOKABLE void setUdp(bool enabled);
  Q_INVOKABLE void resetUdp();

  Q_INVOKABLE ToxTypes::Proxy proxyType() const;
  Q_INVOKABLE void setProxyType(ToxTypes::Proxy type);
  Q_INVOKABLE void resetProxyType();

  Q_INVOKABLE quint16 proxyPort() const;
  Q_INVOKABLE void setProxyPort(quint16 port);
  Q_INVOKABLE void resetProxyPort();

  Q_INVOKABLE QString proxyAddress() const;
  Q_INVOKABLE void setProxyAddress(const QString& ip);
  Q_INVOKABLE void resetProxyAddress();

signals:
  void changed();

private:
  inline void reset(const QString& key) {
    if (s_.contains(key)) {
      s_.remove(key);
      emit changed();
    }
  }
};

} // namespace Toxer
